require 'spec_helper'

describe Fabrication::Support do
  describe '.class_for' do
    context 'with a top level class' do
      it 'can convert a string' do
        expect(described_class.class_for('string')).to eq(String)
      end

      it 'can convert a symbol' do
        expect(described_class.class_for(:string)).to eq(String)
      end

      it 'can handle an exact class name as a string' do
        expect(described_class.class_for('String')).to eq(String)
      end

      it 'can handle a class' do
        expect(described_class.class_for(String)).to eq(String)
      end

      it 'raises for unknown classes' do
        expect { described_class.class_for(:this_is_not_fabricatable) }
          .to raise_error(Fabrication::UnfabricatableError)
      end
    end

    context 'with a nested class' do
      it 'can handle an exact class name as a string' do
        expect(described_class.class_for('OpenSSL::ASN1')).to eq(OpenSSL::ASN1)
      end

      it 'can handle a class' do
        expect(described_class.class_for(OpenSSL::ASN1)).to eq(OpenSSL::ASN1)
      end

      it 'raises for unknown classes' do
        expect { described_class.class_for('ThisIs::NotFabricatable') }
          .to raise_error(Fabrication::UnfabricatableError)
      end
    end

    context 'with a deeply nested class' do
      it 'can handle an exact class name as a string' do
        expect(described_class.class_for('OpenSSL::ASN1::Primitive')).to eq(OpenSSL::ASN1::Primitive)
      end

      it 'can handle a class' do
        expect(described_class.class_for(OpenSSL::ASN1::Primitive)).to eq(OpenSSL::ASN1::Primitive)
      end

      it 'raises for unknown classes' do
        expect { described_class.class_for('ThisIs::Not::Fabricatable') }
          .to raise_error(Fabrication::UnfabricatableError)
      end
    end
  end

  describe '.variable_name_to_class_name', depends_on: :active_support do
    before do
      ActiveSupport::Inflector.inflections(:en) do |inflect|
        inflect.acronym 'OCR'
      end
    end

    it 'handles acronyms correctly' do
      expect(described_class.variable_name_to_class_name('ocr_test')).to eq('OCRTest')
    end
  end

  describe '.hash_class', depends_on: :active_support do
    subject { described_class.hash_class }

    before do
      pending unless defined?(HashWithIndifferentAccess)
    end

    context 'with HashWithIndifferentAccess defined' do
      it { is_expected.to eq(HashWithIndifferentAccess) }
    end

    # rubocop:disable Lint/ConstantDefinitionInBlock, RSpec/LeakyConstantDeclaration, RSpec/RemoveConst
    context 'without HashWithIndifferentAccess defined' do
      before do
        TempHashWithIndifferentAccess = HashWithIndifferentAccess
        described_class.instance_variable_set(:@hash_class, nil)
        Object.send(:remove_const, :HashWithIndifferentAccess)
      end

      after do
        described_class.instance_variable_set(:@hash_class, nil)
        HashWithIndifferentAccess = TempHashWithIndifferentAccess
      end

      it { is_expected.to eq(Hash) }
    end
    # rubocop:enable Lint/ConstantDefinitionInBlock, RSpec/LeakyConstantDeclaration, RSpec/RemoveConst
  end
end
